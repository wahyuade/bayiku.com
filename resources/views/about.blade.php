@extends('layouts.app')
@section('about')
<!--breadcrumbs-->
<div class="breadcrumbs">
  <div class="container">
    <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
      <li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
      <li class="active">About us</li>
    </ol>
  </div>
</div>
<!--//breadcrumbs-->
<!--about-->
	<div class="about">
		<div class="container">
			<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
				<h3 class="title">About<span> Us</span></h3>
				<p>Selamat Datang di Bayiku.com - Pusat Belanja Online Perlengkapan Bayi Terlengkap di Indonesia </p>
			</div>
			<div class="about-info">
				<p>
					Bayiku.com merupakan bagian dari My Baby Group yang menjadi tujuan belanja online perlengkapan bayi nomor satu di Asia Tenggara. My Baby Group beroperasi di Indonesia, Malaysia, Filipina, Singapura, Thailand dan Vietnam. Menjadi pionir di bidang e-commerce perlengkapan bayi, Bayiku.com menghadirkan layanan berbelanja yang mudah bagi konsumen dan akses langsung pada database konsumen terbesar di Asia Tenggara.
				</p>
				<br>
				<br>
				<h3>BERBELANJA MUDAH DI BAYIKU.COM</h3>
				<h4>Tujuan Belanja Utama </h4>
				<p>
					Dengan ratusan ribu pilihan produk tersedia dari berbagai kategori, Bayiku.com menjadi tujuan utama untuk memenuhi kebutuhan berbelanja Anda.
				</p>
				<p>
					Selain dari berbagai pilihan produk dari kualitas terbaik, Anda juga dapat menemukan berbagai produk yang hadir secara eksklusif melalui Bayiku.com.
				</p>
				<h4>Berbelanja Mudah dan Nyaman</h4>
				<p>
					Tanpa harus melalui macet, antrian dan berdesak-desakan! Belanja kapan saja, dimana saja, melalui komputer maupun handphone..
				</p>
				<p>
					Dengan layanan pengiriman kami yang cepat dan dapat diandalkan, Anda hanya perlu duduk santai dan paket akan diantarkan kepada Anda.
				</p>
				<h4>Berbelanja Aman dan Terpercaya</h4>
				<p>
					Kami memahami pentingnya bagi Anda untuk berbelanja dengan aman pada layanan yang dapat dipercaya. Kami menghadirkan berbagai pilihan metode pembayaran bagi konsumen, termasuk bayar tunai di tempat atau Cash-On-Delivery, Anda hanya perlu membayar saat Anda menerima kiriman paket Anda.
				</p>
				<p>
					Kami memastikan kualitas dan keaslian produk: semua produk yang Anda beli di Bayiku.com dijamin asli, bukan barang ilegal dan tidak rusak. Apabila terjadi kasus sebaliknya, Anda dapat mengembalikannya dalam jangka waktu 14 hari dan menerima pengembalian uang sepenuhnya, yang termasuk dalam Program Perlindungan Pelanggan.
				</p>
				<br><br>
				<h3>BERJUALAN MUDAH DI BAYIKU.COM</h3>
				<br>
				<p>
					Dapatkan akses ke database konsumen terbesar di Asia Tenggara dan panduan berjualan yang terintegrasi.
				</p>
				</div>
		</div>
	</div>
	<!--//about-->
@endsection
