@extends('layouts.app')
@section('register')
	<!--login-->
	<div class="login-page">
		<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
			<h3 class="title">Register<span> Form</span></h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur </p>
		</div>
		<div class="widget-shadow">
			<div class="login-top wow fadeInUp animated" data-wow-delay=".7s">
				<h4>Already have an Account ?<a href="signin">  Sign In »</a> </h4>
			</div>
			<div class="login-body">
				<form class="wow fadeInUp animated" data-wow-delay=".7s">
					<input type="text" placeholder="Name" required="" name="name">
					<input type="text" placeholder="Username" required="" name="username">
					<input type="text" name="phone" required="" placeholder="Phone">
					<input type="text" class="email" placeholder="Email Address" required="" name="email">
					<input type="password" name="password" class="lock" placeholder="Password">
					<input type="submit" name="Register" value="Register">
				</form>
			</div>
		</div>
	</div>
	<!--//login-->
@endsection