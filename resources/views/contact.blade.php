@extends('layouts.app')
@section('contact')
<!--breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Contact Us</li>
			</ol>
		</div>
	</div>
	<!--//breadcrumbs-->
	<!--contact-->
	<div class="contact">
		<div class="container">
			<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
				<h3 class="title">How To <span> Find Us</span></h3>
				<p>Selamat Datang di Bayiku.com - Pusat Belanja Online Perlengkapan Bayi Terlengkap di Indonesia </p>
			</div>
			<iframe class="wow zoomIn animated animated" data-wow-delay=".5s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15863.729181666982!2d106.82481017042535!3d-6.2726335480268265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f23b8e465a33%3A0x7ae16c70447e5616!2sPejaten+Bar.%2C+Ps.+Minggu%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta!5e0!3m2!1sid!2sid!4v1468427761906" allowfullscreen=""></iframe>
		</div>
	</div>
	<div class="address"><!--address-->
		<div class="container">
			<div class="address-row">
				<div class="col-md-6 address-left wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<h4 class="wow fadeIndown animated" data-wow-delay=".5s">KRITIK DAN SARAN </h4>
						<form>
							<input class="wow fadeIndown animated" data-wow-delay=".6s" type="text" placeholder="Nama" required="">
							<input class="wow fadeIndown animated" data-wow-delay=".7s" type="text" placeholder="Email" required="">
							<input class="wow fadeIndown animated" data-wow-delay=".8s" type="text" placeholder="Subjek" required="">
							<textarea class="wow fadeIndown animated" data-wow-delay=".8s" placeholder="Kritik dan Saran" required=""></textarea>
							<input class="wow fadeIndown animated" data-wow-delay=".9s" type="submit" value="KIRIM">
						</form>
					</div>
				</div>
				<div class="col-md-6 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>ALAMAT</h4>
						<p>Jl. Kemang Timur No. 22 Pasar Minggu,
							<br>Pejaten Barat
							<br>Jakarta, Indonesia 12510
						</p>
					</div>
					<div class="address-info address-mdl wow fadeInRight animated" data-wow-delay=".7s">
						<h4>TELEPON </h4>
						<p>+6285-736-600-250</p>
						<p>021 223 4570</p>
					</div>
					<div class="address-info wow fadeInRight animated" data-wow-delay=".6s">
						<h4>EMAIL</h4>
						<p><a href="#"> costumer@bayiku.com</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--//contact-->
@endsection
