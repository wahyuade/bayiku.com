@extends('layouts.app')
@section('faq')
<!--breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">FAQ </li>
			</ol>
		</div>
	</div>
	<!--//breadcrumbs-->
	<!--faq-->
	<div class="faq-info">
		<div class="container">
			<div class="title-info">
				<h3 class="title">Frequently Asked<span> Questions</span></h3>
				<p>Selamat Datang di Bayiku.com - Pusat Belanja Online Perlengkapan Bayi Terlengkap di Indonesia </p>
			</div>
			<ul class="faq">
				<li class="item1 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Kapan saya menerima pesanan saya ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Setelah Anda berhasil melakukan pemesanan, tim Customer Service kami akan melakukan proses verifikasi dan akan memberikan informasi terbaru kepada Anda segera melalui email / SMS.</p><p>
							Untuk mengetahui estimasi waktu pengiriman produk Anda, silahkan mengacu pada estimasi waktu yang terletak di halaman masing-masing produk.
						</p></li>
					</ul>
				</li>
				<li class="item2 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Apakah pengiriman dilakukan pada hari Sabtu dan Minggu serta hari libur Nasional ?<span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Mohon maaf, kurir kami tidak mengirimkan produk pesanan pada hari Sabtu dan Minggu serta hari libur Nasional.</p></li>
					</ul>
				</li>
				<li class="item3 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Apakah pengiriman pesanan saya bisa dipercepat ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Mohon maaf, kami tidak dapat mempercepat proses pengiriman. Estimasi pengiriman tergantung dari Supplier/Merchant serta wilayah alamat pengiriman Anda. Kami akan mengirimkan pemberitahuan melalui email dan/atau SMS jika pesanan telah masuk proses pengiriman.</p><p>
							Estimasi maksimum untuk produk internasional adalah 55 hari kalender, sedangkan estimasi waktu pengiriman produk retail dan marketplace dapat dilihat pada keterangan estimasi di masing-masing produk.
						</p></li>
					</ul>
				</li>
				<li class="item4 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Bagaimana saya mengetahui bahwa Saya dapat memilih jenis pengiriman ekonomis untuk pesanan saya ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Apabila lokasi dan berat produk pesanan Anda termasuk dalam kriteria yang dapat menikmati layanan pengiriman ekonomis, maka pilihan tersebut akan muncul saat Anda melakukan pemesanan.</p></li>
					</ul>
				</li>
				<li class="item5 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Bagaimana saya mengetahui bahwa pengiriman ekonomis untuk produk pesanan saya dikenakan biaya pengiriman atau gratis ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Jika Anda dikenakan biaya kirim maka jumlahnya akan tampil pada halaman pemesanan, Jika Anda tidak dikenakan biaya kirim (gratis) maka besaran biaya kirim pada halaman pemesanan akan kosong.</p></li>
					</ul>
				</li>
				<li class="item6 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Apakah Bayiku.com bisa melakukan pengiriman internasional ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Pengiriman internasional tidak tersedia di Bayiku.com untuk saat ini.</p></li>
					</ul>
				</li>
				<li class="item7 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Bolehkah saya membatalkan jika pesanan telah masuk proses pengiriman ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Kami akan berusaha untuk memenuhi permintaan pembatalan Anda dengan menindaklanjuti ke pihak terkait. Untuk permintaan pembatalan, mohon hubungi Customer Service kami di <b>costumer@bayiku.com</b>.</p></li>
					</ul>
				</li>
				<li class="item8 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Apakah ada wilayah tertentu yang tidak masuk area pengiriman Bayiku.com ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Anda harus menyiapkan surat kuasa sebagai konfirmasi dan menyatakan pihak lain sebagai penerima pesanan.Anda juga dapat menghubungi customer service kami di <b>costumer@bayiku.com</b> segera setelah melakukan pemesanan. Pihak kedua harus memperlihatkan tanda pengenal bersamaan dengan surat kuasa dari Anda.</p></li>
					</ul>
				</li>
				<li class="item9 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Kapan tepatnya produk pesanan saya dikirimkan ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Kami belum dapat menginformasikan tanggal dan/atau kapan tepatnya produk pesanan akan dikirimkan. Estimasi pengiriman tergantung dari wilayah pengiriman pesanan Anda dan Supplier/Merchant dari produk yang dipesan serta penjadwalan dari partner kurir logistics kami.</p></li>
					</ul>
				</li>
				<li class="item10 wow fadeInDown animated" data-wow-delay=".5s"><a href="#">Bisakah saya dihubungi jika pesanan akan dikirimkan ? <span class="icon"> </span></a>
					<ul>
						<li class="subitem1"><p>Mohon maaf, kami belum menyediakan fasilitas tersebut.</p></li>
					</ul>
				</li>
			</ul>
			<!-- script for tabs -->
			<script type="text/javascript">
				$(function() {

					var menu_ul = $('.faq > li > ul'),
						   menu_a  = $('.faq > li > a');

					menu_ul.hide();

					menu_a.click(function(e) {
						e.preventDefault();
						if(!$(this).hasClass('active')) {
							menu_a.removeClass('active');
							menu_ul.filter(':visible').slideUp('normal');
							$(this).addClass('active').next().stop(true,true).slideDown('normal');
						} else {
							$(this).removeClass('active');
							$(this).next().stop(true,true).slideUp('normal');
						}
					});

				});
			</script>
			<!-- script for tabs -->
		</div>
	</div>
	<!--//faq-->
@endsection
