<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;

class RegisterController extends Controller
{
    function postregister(Request $req)
    {
        $name = $req->name;
        $username = $req->username;
        $password = $req->password;
        $phone = $req->phone;
        $email = $req->email;
        
        $data = new User();
        $data->name = $name;
        $data->username = $username;
        $data->password = $password;
        $data->email = $email;
        $data->phone = $phone;
        
        $data->save();
        
        return redirect('home');
    }
}
