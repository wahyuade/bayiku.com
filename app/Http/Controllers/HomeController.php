<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Http\Requests;

class HomeController extends Controller
{
    function index(){
       $newitem = Item::get();
       return view('home', compact('newitem'));
    }
    function register(){
        return view('register');
    }
    function signin(){
        return view('signin');
    }
    function contact(){
        return view('contact');
    }
    function faq(){
        return view('faq');
    }
}
