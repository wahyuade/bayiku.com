<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('register', 'HomeController@register');
Route::get('signin', 'HomeController@signin');
Route::get('contact', 'HomeController@contact');
Route::get('faq', 'HomeController@faq');
Route::post('postsignin', 'LoginController@postsignin');
Route::post('postregister', 'RegisterController@postregister');
Route::post('postcontact', 'ContactController@postcontact');
