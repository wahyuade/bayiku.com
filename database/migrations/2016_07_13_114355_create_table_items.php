<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nama_barang',30)->unique();
            $table->text('image');
            $table->text('description');
            $table->float('rating');
            $table->float('item_price');
            $table->float('discount');
        });
    }

    public function down()
    {
        Schema::drop('items');
    }
}
