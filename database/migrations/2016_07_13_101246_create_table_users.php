<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name',30);
            $table->string('username',30)->unique();
            $table->string('email', 30)->unique();
            $table->text('password');
            $table->string('phone', 12)->unique();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
